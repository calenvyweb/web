# For staging server (dev1.calenvy.com)
#
# Staging server is the same as live site (connects to the live database) except that 
# the default site is dev1.calenvy.com

from live import *

SERVER = 'staging'

# Default site for staging: staging.calenvy.com
SITE_ID = 1 

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = '/home/noted/domains/staging.calenvy.com/noted/web/static'

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/static'

UPLOADS_ROOT = '/home/noted/domains/staging.calenvy.com/uploads'

# For the new machine
DATABASE_ENGINE = 'postgresql_psycopg2'     # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = 'staging_noted'             # Or path to database file if using sqlite3.
DATABASE_USER = 'noted'                     # Not used with sqlite3.
DATABASE_PASSWORD = 'u7f6u7b@htbdl'         # Not used with sqlite3.
DATABASE_HOST = 'localhost'                 # Staging server keeps its own db
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.
