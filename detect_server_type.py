import sys,os
from os.path import *
project_root = dirname(abspath(__file__))
server_indicator_file = dirname(project_root) + os.sep + "server_type"

print "server_indicator_file %s" % server_indicator_file

if exists(server_indicator_file):
    fh = open(server_indicator_file)
    server_type = fh.readline().strip()
    fh.close()
    print "server_type: %s" % server_type
else:
    server_type = "local"
    
if server_type not in ["local", "staging", "live"]:
    print "Server type %s unrecognised." % server_type
    sys.exit(1)
