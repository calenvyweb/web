from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
admin.autodiscover()

urlpatterns = patterns('',
    (r'^(?P<subdomain>\w+)/$', 'network.views_cover.account_home'),
    (r'^(?P<subdomain>\w+)/settings/$', 'network.views_cover.settings_view'),
    (r'^(?P<subdomain>\w+)/salesforce/$', 'network.views_cover.salesforce_view'),
    (r'^(?P<subdomain>\w+)/salesforce/(?P<rule_id>\w+)/$', 'network.views_cover.salesforce_view'),

    (r'^(?P<subdomain>\w+)/admin/', include('network.urls_admin')),
    (r'^(?P<subdomain>\w+)/grant_perms/', 'network.perms_request.grant_perms'),
    (r'^(?P<subdomain>\w+)/retrieve_file/', 'network.views_files.retrieve_file'),
    (r'^(?P<subdomain>\w+)/terms/', 'network.views_cover.terms'),
    (r'^(?P<subdomain>\w+)/send_eventresponse_mail/', 'network.views_cover.send_eventresponse_mail'),
    (r'^(?P<subdomain>\w+)/ical/alias/(?P<slug_ics>\w+)', 'network.views_ical.get_alias_ical'),
    (r'^(?P<subdomain>\w+)/contacts_compose/', 'network.ajax_feeds.contacts_compose')
)

