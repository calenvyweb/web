# Celery tasks for network

from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.auth.models import User
from django.contrib import admin
import hashlib
import re, string, os
import json
from utils import *
# from datetime import *
import datetime
from assist.constants import *
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
from assist.models import *
from assist import mail
from schedule.models import *
import pdb, traceback
from paypal.standard.models import *
import crawl.procmail
import urllib, urllib2, sys
import time


class ImportCSVTask(LocalTask):

    def run(self, alias, csv_rows, field_names, category, **kwargs):
        with TaskLogFile(self, kwargs.get('task_id')):

            log("ImportCSVTask start...")
            
            try:
                self.import_csv(alias, csv_rows, field_names, category)
                success = True
            except:
                success = False
    
            log("ImportCSVTask: finished importing, success = ", success)
            
            # Send out success/failure email
            try:
                confirm_subject = _("Finished importing your contacts") if success else \
                    _("Failure importing your contacts")
                assistant = alias.account.get_assistant()       
                c = Context({
                    'success': success,
                    'alias': alias,
                    'account': alias.account,
                    'site': alias.get_site()
                })
                mail.send_email('main_import_finished.html', c, confirm_subject,
                    (assistant.get_full_name(), assistant.email),
                    (alias.get_full_name(), alias.email),
                    noted={'message_type': EMAIL_TYPE_NOTED,\
                        'message_subtype': EMAIL_SUBTYPE_NOTED_IMPORT_SUCCESS if success else EMAIL_SUBTYPE_NOTED_IMPORT_FAIL,
                        'owner_account': assistant and assistant.owner_account
                    }
                )
                
                log("...done!")
            except:
                log(traceback.format_exc())
                log("ImportCSVTask: couldn't send mail")
            
            log("ImportCSVTask: done")

    def import_csv(self, alias, csv_rows, field_names, category):
        
        if category:
            bucket = Bucket.objects.get(account=alias.account, slug=category)
        else:
            bucket = None
              
        n = now()        
        for r in csv_rows:
            log("Row:", r)
                        
            kwargs = {
                'source':   Contact.SOURCE_CSV
            }
            
            notes = []
            for i, field_name in enumerate(field_names):
                if field_name:   
                    # field_name = '' means ignore this field
                    val = r[i]
                    if field_name == 'note' and val:
                        notes.append(val)
                    elif field_name in [c.field_name for c in CONTACT_DEFAULT_FIELDS]:    # some security -- make sure it's a valid field name
                        if val:
                            kwargs[str(field_name)] = val
                            
            # create the contact (if valid email address, otherwise return None)    
            kwargs['viewing_alias'] = alias        
            c, created = Contact.objects.get_or_create_smart(**kwargs)
            
            if c and bucket:
                bucket.contacts.add(c)
                        
            for note in notes:
                # Make a note about this contact in the list. 
                # If a row doesn't refer to a valid contact, just make a note.
                crawl.procmail.procmail_quickbox(alias=alias, content=note, \
                source_subtype = Email.SOURCE_SUBTYPE_CSV_NOTE,
                ref_contact_id=c.id if c else None, \
                ref_bucket_slug=bucket.slug if bucket else None)
                