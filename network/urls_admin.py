from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
admin.autodiscover()

urlpatterns = patterns('',
    (r'^$', 'network.views_admin.admin_view'),
    (r'^approve_email/$', 'network.views_admin.approve_email'),
)
