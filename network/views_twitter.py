try:
        import json
        class Encoder(json.JSONEncoder):
            def default(self, obj):
                try:
                    return obj.to_json()
                except AttributeError:
                    return json.JSONEncoder.default(self, obj)
except ImportError:
        pass

#from publisher.models import *
from django.http import HttpResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.template import RequestContext, Context, loader, Template
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.utils.translation import ugettext as _
from django.db.models import Q
from django.conf import settings
from paypal.standard.signals import payment_was_successful, payment_was_flagged
import hashlib
import re
from datetime import datetime, timedelta
import time
import random
import sys, os, pprint
from utils import *
from django.core import serializers
from assist import mail
from assist.models import *
from twitter.models import *
import traceback, pdb


@global_def    
def twitter_welcome(request, twitteruser_slug=None):
    
    try:
        tu = TwitterUser.objects.get(slug=twitteruser_slug)
        t = loader.get_template('main_twitter_welcome.html')
        c = RequestContext(request, dict= {\
            'success': True,
            'site': get_site(request),
            'twitteruser': tu,
            'noted_twitteruser': TwitterUser.objects.get(is_noted=True, site=get_site(request))
        })
        
        request.session['tolink_twitteruser'] = tu
        log('return httpresponse object')
        c.update(global_context(request))
        return HttpResponse(t.render(c))
    
    except Exception, e:
        log("twitter_welcome: invalid slug", twitteruser_slug)
        return HttpResponseRedirectAgent(request, '/dash/')
  

@global_def    
def twitter_return(request):
    
    try:     
        log("*********** twitter_return")
                                 
        site = get_site(request)
        request_token = request.session['twitter_token']        
        user_tu, created = TwitterUser.objects.get_or_create_by_request_token(request_token, site)
        noted_tu = TwitterUser.objects.get(is_noted=True, site=site)
        
        c = RequestContext(request)
        api = user_tu.get_api()     # the user's Twitter account
        try:
            api.CreateFriendship(str(noted_tu.screen_name))
        except:
            log("Twitter_return: could not create friendship from %s (maybe already befriended)" % user_tu.screen_name)
        
        
        noted_api = noted_tu.get_api()
        try:
            noted_api.CreateFriendship(str(user_tu.screen_name))
        except:
            log("Twitter_return: could not create friendship to %s (maybe already befriended)" % user_tu.screen_name)

        
        if request.session['twitter_return_cmd'] == 'link_alias':
            alias = request.session['alias']
            TwitterUser.objects.link_alias_to_twitteruser(request, alias, user_tu)
        elif request.session['twitter_return_cmd'] == 'link_account':
            alias = request.session['alias']
            if not alias.is_admin:
                raise Exception()
            TwitterUser.objects.link_account_to_twitteruser(request, alias.account, user_tu)
        
        # Refresh the manager's idea of which are the active Twitter accounts
        from network.ajax_feeds import get_widget_manager
        get_widget_manager(request).refresh_manager()
            
        t = loader.get_template('main_twitter_return.html')
        return HttpResponse(t.render(c))
              
    except Exception, e:
        t = loader.get_template('main_twitter_return.html')
        c = RequestContext(request)        
        return HttpResponse(t.render(c))

    

