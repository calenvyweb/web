# Tasks related to Salesforce

from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.auth.models import User
from django.contrib import admin
import hashlib
import re, string, os, cPickle
import json
import BeautifulSoup
from utils import *
# from datetime import *
import datetime
from assist.constants import *
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
from django.template import Context, loader, Template
from schedule.models import *
from remote.models import *
from assist import mail
from email.Utils import formatdate, parsedate_tz, mktime_tz
from django.db.models import Q
from models import *
import pdb, traceback
import urllib, urllib2, sys
import time
from celery.task import *
from celery.decorators import task
import multiprocessing
import calendar
import sys, os
import csv, copy
import smtplib

patch_urllibs()

# Dummy task for testing Celery
@task
def print_celery(s):
    print s
    
    
def render_user_template(alias, contact, subject, message, include_unsubscribe_link=False, unsubscribe_email_id=None, parse=True, event=None):
    # alias is the alias sending this formmail
    # contact is a recipient
        
    c = Context({
        'name':             ' '.join([str_or_none(contact.first_name), str_or_none(contact.last_name)]).strip(),
        'first_name':       str_or_none(contact.first_name),
        'last_name':        str_or_none(contact.last_name),
        'email':            str_or_none(contact.email),
        'company':          str_or_none(contact.company),
        'website':          str_or_none(contact.website),
        'zip_code':         str_or_none(contact.zip_code),
        'phone':            str_or_none(contact.phone),
        'description':      str_or_none(contact.description),
        'id':               str_or_none(contact.pk),
        'my_name':          str_or_none(' '.join([str_or_none(alias.contact.first_name), str_or_none(alias.contact.last_name)]).strip()),
        'my_first_name':    str_or_none(alias.contact.first_name).strip(),
        'my_last_name':     str_or_none(alias.contact.last_name).strip(),
        'my_email':         str_or_none(alias.email)
    }, autoescape=False)
    
    # Do NOT use the global context here!
    try:
        body_template = Template('{% load formmail_tags %}' + message)
        subject_template = Template('{% load formmail_tags %}' + subject)
        
        rendered_body = body_template.render(c).strip()
        if include_unsubscribe_link:
            rendered_body += '\n\n----\n'
            rendered_body += _('To unsubscribe from mails from %s, please visit this link:') % alias.get_full_name_or_email(include_email=True) + '\n'
            
            if unsubscribe_email_id:
                url = 'http://%s/unsubscribe/?slug=%s&mail_id=%s\n' % (alias.account.site.domain, contact.unsubscribe_slug, unsubscribe_email_id)
            else:
                url = 'http://%s/unsubscribe/?slug=SAMPLE&mail_id=12345' % alias.account.site.domain
            
            rendered_body += url + '\n'
        rendered_subject = subject_template.render(c).strip()
        
        if event and event.status in [EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE]:
            from assist import ics
            ics = [ics.ics_from_events([event], description=rendered_subject+'\n'+rendered_body, method=None, to=contact)]
        else:
            ics = []
             
        return rendered_body, rendered_subject, True, ics
    except:
        return '', '', False, None
    

class SendFormMailTask(LocalTask):
    
    def run(self, alias, args, noted_meta=None, **kwargs):
        with TaskLogFile(self, kwargs.get('task_id')):
            log("SendFormMailTask start...")
            
            if noted_meta:
                noted_meta.mark_started()
            ############################################## :)
            self.send_form_mails(alias, args)
            
            if noted_meta:
                noted_meta.mark_finished()
                
            log("SendFormMailTask: done")
    
    def send_form_mails(self, alias, args):
        log("SendFormMailTask.send_form_mails...")
        
        try:
            contacts_sent, contacts_notsent = [], []          # Which contacts we've successfully sent mails to
            log("1")
            
            assistant = alias.account.get_assistant()
            
            contact_ids = args['contact_ids']
            template = args['template']
            subject = args['subject']
            from_type = args['from']
            if 'original_email_id' in args:
                original_email = Email.objects.get(pk=args['original_email_id'])
            else:
                original_email = args.get('original_email', None)
            include_unsubscribe_link = args.get('include_unsubscribe_link', False)
            try:
                # If a scheduled event is part of the email, send out an .ics with each form mail
                event = original_email.events.get(status__in=[EVENT_STATUS_SCHEDULED, EVENT_STATUS_TENTATIVE])
            except:
                # No schedule events
                event = None
            
            log("from_type:", from_type)
            log("original_email:", unicode(original_email).encode('utf-8'))
            
            if from_type not in ['alias', 'assistant', 'assistant_sender']:
                raise Exception('send_form_mails: unknown from_type %s' % from_type)
            if from_type in ['alias', 'assistant_sender']:
                fromm=alias
#                if (alias.account and alias.server_smtp_enabled):
#                    log("Setting fromm to alias")
#                    fromm=alias
#                else:
#                    raise Exception('send_form_mails: cannot send from %s without SMTP enabled' % alias.email)
            else:
                log("Setting fromm to assistant")
                fromm=assistant
            
            if from_type == 'alias' and not alias.server_smtp_enabled:
                raise Exception('send_form_mails: cannot send from %s (without a Sender: field) unless SMTP enabled' % alias.email)

            log("contact_ids:", contact_ids)
            log("template", template)
                        
            contacts = Contact.objects.viewable(alias).filter(id__in=contact_ids).exclude(email=None).exclude(email='').filter(subscribed=True)
            contacts_tosend = list(contacts)
            if from_type == 'alias':
                connection = alias.get_smtp_connection()
            else:
                connection = None
            num_attempted_sent = 0

        except:
            log("formmail: failure")
            log(traceback.format_exc())
            return
        
        try:#################################################### :)
            while False: #contacts_tosend:
                contact = contacts_tosend[0]            
                message_rendered, subject_rendered, template_good, ics = render_user_template(alias, contact, subject, template, \
                    include_unsubscribe_link=include_unsubscribe_link, \
                    unsubscribe_email_id=original_email.pk, event=event)
                if template_good:
                    success = False
                    retry_intervals, num_tries = FORMMAIL_RETRY_INTERVALS[:], 1
                    while retry_intervals:
                        log("formmail: sending from %s to %s (try %d)" % (alias.email, contact.email, num_tries))
                        try:
                            if connection: 
                                connection.open()                   # Does nothing if connection already open
                            result = mail.send_email(None, None, subject_rendered,
                                (fromm.get_full_name(), fromm.email),
                                [(contact.get_full_name(), contact.email)],
                                reply_to = (alias.get_full_name(), alias.email) if alias.server_enabled else (assistant.get_full_name(), assistant.email),
                                alias=alias,
                                sender=(assistant.get_full_name(), assistant.email) if from_type=='assistant_sender' else None,
                                content = message_rendered, 
                                content_type='text/plain',
                                noted={'message_type': EMAIL_TYPE_NOTED,\
                                       'message_subtype': EMAIL_SUBTYPE_NOTED_FORWARD,
                                       'owner_account': alias.account, 
                                       'original_email': original_email
                                },
                                ics=ics,
                                connection=connection           # Keep one connection open the whole time if possible
                            )
                            if result:
                                contact.update_last_contacted_date(alias)
                                contact.save()
                                success = True
                                contacts_sent.append(contact)
                                break 
                        ######################################################### :)
			#except: #smtplib.SMTPRecipientsRefused:   # Couldn't send to that specific recipient, otherwise everything's ok
                        #    pass                                # Just skip this contact
                        except:
                            try:                                # Try to close the connection, next time we retry connection will be opened again
                                if connection:
                                    connection.close()          
                            except:
                                pass   
                        time.sleep(retry_intervals.pop(0)) # convert minutes to seconds
                        num_tries += 1
                    if not success:
                        log("formmail: Reached max retries on sending")
                        raise Exception("Reached max retries, giving up")    
                # job.mark_updated()
                
                contacts_tosend.pop(0)
                num_attempted_sent += 1
                log("num_attempted_sent: ", num_attempted_sent)
#                log(num_attempted_sent % HOWMANY_FORMMAIL_EMAILS_AT_ONCE == 0)
#                if (num_attempted_sent % HOWMANY_FORMMAIL_EMAILS_AT_ONCE == 0):
#                    log("formmail: pausing for %s seconds after %s mails attempted sent" % (HOWLONG_FORMMAIL_DELAY_BETWEEN_BATCHES, num_attempted_sent))
#                    time.sleep(HOWLONG_FORMMAIL_DELAY_BETWEEN_BATCHES)
#                    log("formmail: pause done")
#                    # raise Exception()            # for testing the failure email only
#            
            success = True

        

        except:
            log("--3--")
            success = False
            log(traceback.format_exc())
        
        try: 
            if connection:   
                connection.close()
        except:
            pass
        
        # Send out success/failure email
        try:
            if len(contacts_sent) == 1:
                # If just one person was sent out, confirmation should have the actual content sent out instead of the template
                template, subject_rendered, template_good, ics = render_user_template(alias, contacts_sent[0], subject, template)
              
            confirm_subject = _("Finished sending your form mail") if success else \
                _("Failure sending your form mail")
            if subject.strip():
                confirm_subject += ': ' + subject.strip()
            assistant = alias.account.get_assistant()       
            contacts_notsent = [c for c in contacts if c not in contacts_sent]
            contacts_sent.sort(key=lambda x: x.get_to_for_display())
            contacts_notsent.sort(key=lambda x: x.get_to_for_display())
            c = Context({
                'success': success,
                'alias': alias,
                'account': alias.account,
                'contacts_sent': contacts_sent,
                'contacts_notsent': contacts_notsent,
                'subject':  subject_rendered,
                'template': template,
                'site': alias.get_site()
            })
            mail.send_email('main_formmail_finished.html', c, confirm_subject,
                (assistant.get_full_name(), assistant.email),
                (alias.get_full_name(), alias.email),
                noted={'message_type': EMAIL_TYPE_NOTED,\
                    'message_subtype': EMAIL_SUBTYPE_NOTED_FORMMAIL_SUCCESS if success else EMAIL_SUBTYPE_NOTED_FORMMAIL_FAIL,
                    'owner_account': assistant and assistant.owner_account
                }
            )
            
            log("...done!")
            
        except:
            # Failure.
            log("formmail - couldn't send out success/failure message")
            log(traceback.format_exc())


class PurgeEmailsForAccountTask(LocalTask):
    "A task that does the nightly deletion of emails older than the retention settings."
    
    def delete_email(self, e):
        try:
            for f in e.get_files():
                f.delete()          # this also deletes the file from Mosso, if needed
                
            # e's ForeignKeys to other emails may no longer be valid -- set them to None before
            e.original_parent = None
            e.original_email = None
            e.parent = None
            
            # Unlink email from the children, so it doesn't cause a cascade of deletions
            for c in e.original_email_children.all():
                c.original_parent = None
                c.save()
            for c in e.email_children.all():
                c.parent = None
                c.save()
            for event in e.events.all():
                for child_event in event.event_children.all():
                    child_event.parent = None
                    child_event.save()
            e.delete()
             
        except:
            log(traceback.format_exc())
            log("Can't delete email %d" % e.pk)     
        
    def run(self, account, **kwargs):
        with TaskLogFile(self, kwargs.get('task_id')):
            log("PurgeEmailsForAccountTask start: account = ", account.base_url)
                        
            n = now()
            for alias in Alias.objects.filter(account=account): # Don't filter for active aliases here, either
                log(account.base_url + ': alias: %s' % alias.email)
                # Don't purge emails for accounts in Archive mode, ever
                if (account.store_email_howlong != -1 and account.store_email_howlong > 0) and not (account.archive_mode):
                    log(account.base_url + ': account.store_email_howlong: %s' % account.store_email_howlong)
                    # Delete emails older than the retention threshold, except for:
                    #  * those about future events
                    #  * those typed into the quickbox
                    #  * those that have been synced to Salesforce
                    #  * those that have an active (non-team) contact [can't filter on this using a queryset, the exclude doesn't work the right way]
                    # Get just a list of IDs, since the full set of emails may be too big for memory
                                        
                    email_ids_to_delete = Email.objects.filter(owner_account=account, owner_alias=alias, date_sent__lte=now()-ONE_DAY*account.store_email_howlong)\
                        .exclude(events__status__in=[EVENT_STATUS_TENTATIVE, EVENT_STATUS_SCHEDULED], events__end__gte=n) \
                        .exclude(source=Email.SOURCE_QUICKBOX) \
                        .exclude(source=Email.SOURCE_CRAWLED, email2remoteuser__status=Email2RemoteUser.STATUS_SYNCED, email2remoteuser__remote_user__type=RemoteUser.TYPE_SF) \
                        .values_list('id', flat=True).order_by('id')
                                                                    
                    email_ids_to_delete = uniqify(email_ids_to_delete)
                    log(account.base_url + ': email_ids_to_delete: %s' % email_ids_to_delete)
                    
                    for email_id in email_ids_to_delete:
                        try:
                            e = Email.objects.get(id=email_id)
                            
                            # Now, test for emails that have at least one active non-team contact -- we don't want to purge those
                            if e.contacts.filter(status=Contact.STATUS_ACTIVE, alias=None).count() == 0:
                                log (" ... deleting email ", account.base_url, e.pk, e.date_sent)
                                self.delete_email(e)
                            else:
                                log (" ... NOT deleting email because contains an active contact: ", account.base_url, e.pk, e.date_sent)
                        except:
                            log(traceback.format_exc())
                            
                    # purging emails from assistant, etc.
                    
                    log (" ... deleting owner_alias=None emails for ", account.base_url)
                    email_ids_to_delete = Email.objects.filter(owner_account=account, owner_alias=None, date_sent__lte=now()-ONE_DAY*account.store_email_howlong)\
                        .exclude(events__status__in=[EVENT_STATUS_TENTATIVE, EVENT_STATUS_SCHEDULED], events__end__gte=n).values_list('id', flat=True).order_by('id')
                    email_ids_to_delete = uniqify(email_ids_to_delete)
                    for email_id in email_ids_to_delete:
                        try:
                            e = Email.objects.get(id=email_id)
                            log (" ... deleting email ", account.base_url, e.pk, e.date_sent)
                            self.delete_email(e)
                        except:
                            log(traceback.format_exc())
                    
            else:
                log (" ... retaining owner_alias=None emails for ", account.base_url, " because store_email_howlong is -1 (store forever)")    
            
            log("PurgeEmailsForAccountTask: done")
        
        
class FollowupSalesforceInstallationsTask(LocalTask):
    """Check for anyone who's installed any of our Salesforce apps in the last 15 minutes and
    schedule an email to be sent to them from the followup server."""
    
    def run(self, *args, **kwargs):
        with TaskLogFile(self, kwargs.get('task_id')):
            log("FollowupSalesforceInstallationsTask start...")
                        
            do_it = kwargs.get('do_it', True)
            
            try:
                import beatbox
                svc = beatbox.PythonClient()
            except:
                import mail
                log("FollowupSalesforceInstallationsTask: Error! Beatbox is not installed!")
                mail.mail_admins('FollowupSalesforceInstallationsTask: Error! Beatbox is not installed on Cron server!')
                return         
            try:
                log("FollowupSalesforceInstallationsTask - attempting login: %s %s" % (settings.SFDC_LOGIN_EMAIL,settings.SFDC_LOGIN_TOKEN))
                svc.login(settings.SFDC_LOGIN_EMAIL, settings.SFDC_LOGIN_PASSWORD + settings.SFDC_LOGIN_TOKEN)
                log("FollowupSalesforceInstallationsTask svn.login status: %s" % svc.login)
            except:
                # Can't login, need to update the credentials in settings.py
                import mail
                mail.mail_admins('settings.py - update Salesforce password/token', 
                    'FollowupSalesforceInstallationsTask: can\'t login to Salesforce, please update your password and token in settings.py.')
                return
                        
            query = "select Id, CreatedDate, FirstName, LastName, Company, LeadSource, Status, Email from Lead "
            query += "where Status = 'Open - Not Contacted' and LeadSource like 'SFDC-%' and "            
            date_from = kwargs.get('since') or (now() - 15*ONE_DAY)
            query += "CreatedDate > %s" % date_from.strftime('%Y-%m-%dT%H:%M:%S-00:00')
            results = svc.query(query)['records']
            records_to_upsert = []
                        
            for r in results:
                source = r['LeadSource']
                if source.startswith('SFDC') and '|' in source:
                    c = Contact(
                        first_name = r['FirstName'],
                        last_name = r['LastName'],
                        company = r['Company'] if (r['Company'].lower().strip() != '[not provided]') else '',
                        email = r['Email'],
                    )
                    
                    from network.ajax_admin import admin_followup
                    product_name = source.split('|')[1].strip()
                    # Choose one matching campaign for this item
                    for key, value in settings.SFDC_FOLLOWUP_CAMPAIGNS.items():
                        if re.match(key, product_name):                            
                            if settings.SERVER == 'live' and do_it:
                                                        
                                log("FollowupSalesforceInstallationsTask: Sending %s to followup server: " % r['Id'], c, value, source)
                                admin_followup(c, value, source=source)
                                
                                log("FollowupSalesforceInstallationsTask: marking %s as 'Working - Contacted'" % r['Id'])
                                try:
                                    u = {
                                         'type':        'Lead',    
                                         'Id':          r['Id'],
                                         'Status':      'Working - Contacted'
                                    }
                                    svc.update(u)
                                except:
                                    log (" -- couldn't mark as Working - Contacted")
                                
                            else:
                                log("FollowupSalesforceInstallationsTask: not actually sending to followup server:", c, value, source)
                            break
                            
class LogoDownloadTask(LocalTask):
    """Download account logo images, where the logo_url is on a remote site,
    and store the logos locally.
    MUST be run as a user who has write permissions to the uploads folder!"""
    
    def run(self, *args, **kwargs):
        with TaskLogFile(self, kwargs.get('task_id')):  
                
            log("cron logo_download...")
            for account in Account.objects.filter(Q(logo_url__startswith='http://') | Q(logo_url__startswith='https://')):
                try:
                    log("Downloading logo image for account %s" % account.base_url)
                    req = urllib2.Request(account.logo_url)
                    response = urllib2.urlopen(req)
                    data = response.read()
                    extension = os.path.splitext(account.logo_url)[1]
                    output_file = os.path.join(settings.STATIC_LOCAL_DIR, 'screencast', 'accounts', '%s%s' % (account.base_url, extension))
                    f = open(output_file, 'wb')
                    f.write(data)
                    f.close()
                    account.logo_url = '/static/local/screencast/accounts/%s%s' % (account.base_url, extension)
                    account.save()
                except:
                    log("Couldn't download logo for account %s" % account.base_url)
                    
            # Download company names from Google Mail, if they exist...
            for account in Account.objects.filter(name=None, date_created__gte=now()-3*ONE_DAY, alias__email__contains='@'):
                domain = account.alias.all()[0].email.split('@')[-1].lower()
                if not is_common_base_url(domain.split('.')[0]):            
                    try:
                        log("Trying to fetch company name for %s..." % domain)
                        req = urllib2.Request('http://mail.google.com/a/%s' % domain)
                        response = urllib2.urlopen(req)
                        html = response.read()
                        soup = BeautifulSoup.BeautifulSoup(html)
                        name = soup.find('div', 'loginBox').find('h2').contents[0].strip()
                        name = name.split('.')[0]
                        name = decode_htmlentities(name)
                        if name:
                            log("Found name: %s" % name)
                            account.name = name
                            account.save()
                    except:
                        log("No name for %s" % domain)
                
            # Download logos from Google Mail, if they exist...
            #  For these images, we always know it's a .png
            for account in Account.objects.filter(logo_url=None, date_created__gte=now()-3*ONE_DAY, alias__email__contains='@'):
                domain = account.alias.all()[0].email.split('@')[-1].lower()
                if not is_common_base_url(domain.split('.')[0]):                             
                    try:
                        log("Trying to fetch Google Mail logo for %s..." % domain)
                        req = urllib2.Request('https://www.google.com/a/cpanel/%s/images/logo.gif?service=mail' % domain)
                        response = urllib2.urlopen(req)
                        image = response.read()
                        if hashlib.md5(image).hexdigest() not in ['722e49a3457b1a1efb547034f5270ba3', '1d82345d905927cdd870ab359bf1a497']:    # Don't grab Google's generic logo
                            output_file = os.path.join(settings.STATIC_LOCAL_DIR, 'screencast', 'accounts', '%s.png' % account.base_url)
                            log("... found! saving as %s" % output_file)
                            f = open(output_file, 'wb')
                            f.write(image)
                            f.close()
                            if os.path.exists(output_file):
                                account.logo_url = '/static/local/screencast/accounts/%s.png' % account.base_url
                                account.save()
                        else:
                            log("... generic Gmail logo, skipping")
                    except:
                        log("... no logo exists")
                    
            
    
class GetSocialDataTask(LocalTask):
    "Try to get social data for all contacts created in the last 2 days that don't have social data."
    
    def run(self, *args, **kwargs):
        with TaskLogFile(self, kwargs.get('task_id')):
            log("GetSocialDataTask: start")
            for c in Contact.objects.filter(created_date__gte=now()-2*ONE_DAY, social_json=None, alias=None):
                log("Updating social data for ", c)
                c.update_social_data()
            log("GetSocialDataTask: end")
            
                    
# Various quick launchers for the tasks in this file

# Run this at midnight
def purge_emails_all():
    i = 1
    for account in Account.objects.all():       # Don't filter for active accounts here! We purge emails from all accounts.
        if account.archive_mode:
            log("** purge_emails_all: avoiding account", account.base_url, " because archive_mode=True")
        else:
            log("** purge_emails_all: account = ", account.base_url, i)
            PurgeEmailsForAccountTask().delay(account)
            i += 1


        
