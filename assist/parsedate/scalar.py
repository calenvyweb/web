import re
from tokens import *

class Scalar(Tag):
    @staticmethod
    def scan(tokens):
        for i in range(len(tokens)):
            token = tokens[i]
            post_token = tokens[i+1] if i < len(tokens)-1 else None
            tokens[i].tag(Scalar.scan_for_scalars(token, post_token))
            tokens[i].tag(Scalar.scan_for_days(token, post_token))
            tokens[i].tag(Scalar.scan_for_months(token, post_token))
            tokens[i].tag(Scalar.scan_for_years(token, post_token))
        return tokens
            
    @staticmethod
    def scan_for_scalars(token, post_token):
        if re.match(r"^\d{1,4}$", token.word):
            if not (post_token in 'am pm morning afternoon evening night'.split()):
                return Scalar(int(token.word))
        return None
    
    @staticmethod
    def scan_for_days(token, post_token):
        if re.match(r"^\d\d?$", token.word) and int(token.word) <= 31:
            if not (post_token in 'am pm morning afternoon evening night'.split()):
                return ScalarDay(int(token.word))
        return None
        
    @staticmethod
    def scan_for_months(token, post_token):
        if re.match(r"^\d\d?$", token.word) and int(token.word) <= 12:
            if not (post_token in 'am pm morning afternoon evening night'.split()):
                return ScalarMonth(int(token.word))
        return None
    
    @staticmethod
    def scan_for_years(token, post_token):
        if re.match(r"^([1-2]\d)?\d\d$", token.word):
            if not (post_token in 'am pm morning afternoon evening night'.split()):
                year = int(token.word)
                if year < 100:
                    return ScalarYear(2000 + year)
                else:
                    return ScalarYear(year)
        return None
  
    def __repr__(self):
        return 'S:%s' % str(self.data)
        
class ScalarDay(Scalar):
    def __repr__(self):
        return 'Sd:%s' % str(self.data)
            
class ScalarMonth(Scalar):
    def __repr__(self):
        return 'Sm:%s' % str(self.data)
                 
class ScalarYear(Scalar):
    def __repr__(self):
        return 'Sy:%s' % str(self.data)
        