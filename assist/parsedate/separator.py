import re
from tokens import *

class Separator(Tag):
    @staticmethod
    def scan(tokens):
        for token in tokens:
            token.tag(Separator.scan_for_commas(token))
            token.tag(Separator.scan_for_slash_or_dash(token))
            token.tag(Separator.scan_for_begin(token))
            token.tag(Separator.scan_for_in(token))
            token.tag(Separator.scan_for_combiner(token))
            token.tag(Separator.scan_for_between(token))
            token.tag(Separator.scan_for_separator_time(token))
            token.tag(Separator.scan_for_separator_date(token))
            
    @staticmethod
    def scan_for_commas(token):
        if re.match(r"^,$", token.word):
            return SeparatorComma(PD_COMMA)
        return None
        
    @staticmethod
    def scan_for_slash_or_dash(token):
        if re.match(r"^\/$", token.word):
            return SeparatorSlashOrDash(PD_SLASH)
        if re.match(r"^-$", token.word):
            return SeparatorSlashOrDash(PD_DASH)    
        return None

    @staticmethod
    def scan_for_begin(token):
        if re.match(r"^(at|@)$", token.word):
            return SeparatorBegin(PD_AT)
        if re.match(r"^(from)$", token.word):
            return SeparatorBegin(PD_FROM)
        return None
        
    @staticmethod
    def scan_for_in(token):
        if re.match(r"^in$", token.word):
            return SeparatorIn(PD_IN)
        if re.match(r"^of$", token.word):       # Added [Emil]
            return SeparatorIn(PD_OF)
        return None
    
    @staticmethod
    def scan_for_separator_time(token):
        if re.match(r"^(at|@)$", token.word):
            return SeparatorTime(PD_AT)
        if re.match(r"^by$", token.word):       # Added [Emil]
            return SeparatorTime(PD_BY)
        return None    
        
    @staticmethod
    def scan_for_separator_date(token):
        if re.match(r"^on$", token.word):
            return SeparatorDate(PD_ON)
        if re.match(r"^by$", token.word):       # Added [Emil]
            return SeparatorDate(PD_BY)
        return None
                
    @staticmethod
    def scan_for_combiner(token):
        # if re.match(r"^or$", token.word):
        #     return SeparatorCombiner(PD_OR)
        # elif re.match(r"^and$", token.word):
        #     return SeparatorCombiner(PD_AND)
        return None    
        
    @staticmethod
    def scan_for_between(token):
        # if re.match(r"^between$", token.word):
        #     return SeparatorBetween(PD_BETWEEN)
        return None
            
    def __repr__(self):
        return '_:%s' % str(self.data)

class SeparatorComma(Separator):
    pass
    
class SeparatorSlashOrDash(Separator):
    pass

class SeparatorBegin(Separator):
    pass

class SeparatorIn(Separator):
    pass

class SeparatorCombiner(Separator):
    pass

class SeparatorTime(Separator):
    pass
    
class SeparatorDate(Separator):
    pass
            
                