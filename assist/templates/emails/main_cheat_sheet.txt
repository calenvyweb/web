{% load i18n %}{% trans 'Hello, ' %}{{alias.get_full_name_or_email}} -- {% trans 'this is your cheat sheet.' %}

Interaction with your team happens via email (or via the website: http://{{alias.account.base_url}}.{{site.domain}}).  
Email your team assistant at {{assistant.email}} to add a note to your dashboard (try: "arriving in town tonight @ 9pm @all" - this includes everyone in an event @ 9pm).

-- Daily Digest

To receive your digest (the email containing all of your reminders and team status updates), mention "send @digest" in the subject or body.  You can change your digest preferences by logging on: http://{{alias.account.base_url}}.{{site.domain}}).

-- Scheduling

To setup a simple event, try emailing any of the following to {{assistant.email}}. The text can be placed in the subject or body of the email:

"Meeting today @ 9am with California Bank regarding @funding" -- this tags the event with "funding"
"Meeting next Friday @ 6pm with John Shelp and @nadine" -- Nadine gets a notification and reminders of the event

-- Multiple times in an email

{{site.name}} does not currently understand how to process multiple dates or times in emails.  Use an @ symbol next to the time
you would like added to your calendar to make events more obvious to your assistant.

-- Tagging

Anything or anyone can be tagged using the @tag syntax.  Tagging helps you organize your events and is a quick way to reference a person on your team.  Try adding "@tag" to your email subject or message body; you can also add tags by sending your email to tagname@{{alias.account.base_url}}.{{site.domain}}:
You can tag specific people in your network, example: @james will tag your team-mate "James Brown <james.brown@{{alias.email_domain}}>" in any of your notes.

Two ways to tag your notes:

    1. Using a tag via the To address, tag@{{site.domain}}.

        From: {{alias.email}}
        To: bugs@{{alias.account.base_url}}.{{site.domain}} <-- tag: bugs
        Subject: cannot login to site with username "flemming" 

    2. Using the @tag syntax in your email to {{assistant.email}}.

        From: {{alias.email}}
        To: {{assistant.email}}
        Subject: Get promotional materials @ today for sales demo @sales with @james <-- tag: sales. tag-time: today. tag-team: James Brown (@james)

-- Emailing everyone

Just send an email to all@{{alias.account.base_url}}.{{site.domain}} and everyone at {{alias.email_domain}} receives an email blast with your message.  
Alternatively, you can simply use the @all tag in any of your notes or events and everyone will receive a notification.

-- Inviting others to your team at {{alias.account.base_url}}

Send an email to {{assistant.email}}; try: "invite bob.jones@email.com, dave.johnson@email.com"

-- Privacy

To create a private event with Mike Jones and/or Jenn, simple email {{assistant.email}} with:

	"Meeting with @mike tonight at 9pm @private"
	"Meeting with @mike and @jenn next Friday @p" <-- Notice @p and @private are interchangeable 

-- Additional help

Are we missing something in this cheat sheet?  Have a feature request, or an idea for making {{site.name}} even easier? Send us an email: founders@{{site.domain}}

-- Shortcuts

The following tags can be used and are interchangeable:

    @d or @digest -- used to send your digest
    @p, @private, or "confidential" -- used to mark something as confidential
