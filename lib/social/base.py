import urllib, urllib2, hashlib, BeautifulSoup, cookielib, threading, cPickle, json
from django.core.cache import cache

DEFAULT_IMAGE_SIZE = 80

class BaseFinder(object):
    "Base class for looking contact info up in various social networks."
    
    def __init__(self, default_image_size=None, default_image_url=None):
                            
        self.default_image_size = default_image_size or DEFAULT_IMAGE_SIZE   # the default image size to get
        self.default_image_url = default_image_url                           # if no avatar found, point to this URL (needed for Gravatar)
    
            
    def find(self, d):
        """Given a dictionary with some lookup keys,
        return a dictionary with info from the given source
        (format of that dictionary is specific to the source)
        Returns {} if no info on that user from that source.
    
        Lookup keys are:
            'email'
            'first_name'
            'last_name'
            'name'
        """
            
        raise NotImplementedError       # abstract method
    
    
    def _hash(self, d):
        "Create an md5 hash of a dictionary, used for caching (keys are sorted in alphabetical order)"
        
        keys = d.keys()
        keys.sort()
        str_to_hash = ','.join(['%s=%s' % (k, d[k]) for k in keys])
        return hashlib.md5(str_to_hash.encode('utf-8')).hexdigest()
            
    
    def cached_find(self, d):
        """
        A wrapper for find that uses memcached
        """
        
        cache_key = 'social.%s.%s' % (self.name, self._hash(d))
        val = cache.get(cache_key)
        if val:
            return cPickle.loads(str(val))
        else:
            # Not in memcached, need to do the actual find
            result = self.find(d)
            cache.set(cache_key, cPickle.dumps(result))
            return result
                

# thanks to http://stackoverflow.com/questions/1023224/how-to-pickle-a-cookiejar/1023245#1023245
class PicklableCookieJar(cookielib.CookieJar):
    "CookieJar subclass that can be pickled - use this to store cookies in memcached"
    def __getstate__(self):
        state = self.__dict__.copy()
        del state['_cookies_lock']
        return state

    def __setstate__(self, state):
        self.__dict__ = state
        self._cookies_lock = threading.RLock()
        