"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""

#from django.test import TestCase
from gravatar import GravatarFinder
from rapleaf import RapleafFinder
from linkedin import LinkedInFinder

class SimpleTest():
    
    def test_gravatar(self):

        gf = GravatarFinder()
        d = {'email': 'foo@crwdimwidmf.com'}
        d, new = gf.find(d)
        assert(not new)
        #self.failIf(new, 'Should be no results for this email address')
        
        d = {'email': 'calenvy@calenvy.com'}
        d, new = gf.find(d)
        assert(new)
        #self.failUnless(new, 'Should be an image for this email address')
        
    def test_rapleaf(self):
        
        rlf = RapleafFinder()
        d = {'email': 'foo@crwdimwidmf.com'}
        d, new = rlf.find(d)
        assert(not new)
        #self.failIf(new, 'Should be no results for this email address')
        
        d = {'email': 'calenvy@calenvy.com'}
        d, new = rlf.find(d)
        assert(new)
        
    def test_linkedin(self):
        
        lf = LinkedInFinder()
        d = {'name': 'Blasdfasdf Wjofiejf'}
        d, new = lf.find(d)
        assert(not new)
        #self.failIf(new, 'Should be no results for this name')
        
        d = {'name': 'Barack Obama'}
        d, new = lf.find(d)
        assert(new)
        
        
__test__ = {"doctest": """
Another way to test that 1 + 1 is equal to 2.

>>> 1 + 1 == 2
True
"""}

