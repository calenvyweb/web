from django.conf.urls.defaults import *
from django.conf import settings
from django.contrib import admin
from django.conf.urls import *
import network.views_twitter

urlpatterns = patterns('',
    (r'^return/$', 'network.views_twitter.twitter_return'),
    (r'^(?P<twitteruser_slug>\w+)', 'network.views_twitter.twitter_welcome')
)

