from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.template import Context, loader, Template
from remote.models import *
from models import *
from django.db.models import Q
from django.db import transaction
from django.conf import settings
import pdb, traceback
import sys, os, htmlentitydefs, hashlib, json, re, sched, time, threading, random, urllib, copy
from utils import *
from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.http import HttpRequest
from django.contrib.auth import authenticate, login, logout
import json, re
from openid.consumer import consumer
from openid.extensions import ax

    

def openid_start(request):
    """
    First step in OpenID authentication: Return a page which appears in the popup window,
    which makes an Ajax call to the OpenID provider to get the URL of the authentication page"""
    
    try:
        email = request.GET['email']
        log("openid_start: email = ", email)
        
        c = RequestContext(request, dict={
            'email': email                              
        })
        
        t = loader.get_template('openid_start.html')
        
        #transaction.commit()                    # in case a registration took place
        return HttpResponse(t.render(c))    
    
    except:
        return HttpResponseRedirect('/')
    
  
   
def _login_alias(request, alias):
    # Log the user in whether they're active or not, but
    #  inactive users will only be able to go to the upgrade page
    #  (The popup will take inactive users to the upgrade page,
    #  but the sidebar will display the login page again)
    clear_session(request)
    user = auth.authenticate(user=alias.user)
    auth.login(request, user)
    alias.last_login_date = now()
    log("Setting session key [1]:", request.session.session_key)
    alias.session_key = request.session.session_key
    alias.save()                            
    request.session['alias'] = alias  
        
    if alias.account.status == ACCOUNT_STATUS_ACTIVE:                      
        d = {'success': True, 'email': alias.email, 'inactive': False}
    else:
        d = {'success': False, 'email': alias.email, 'inactive': True}

    return d


#@transaction.commit_manually
def openid_return(request):
    """
    Second step in OpenID authentication: OpenID provider returns the user to this URL, which logs in the user"""
    
    try:               
        # Stateful OpenID consumer:
        #    request.session is where we store the per-user state (for an AnonymousUser who hasn't logged in yet)
        #    None - we'll establish a shared secret every time, instead of storing it in our DB (for now)
                                
        c = consumer.Consumer(request.session, None)     
        
        site = Site.objects.get_current()
        domain = 'http%s://%s' % ('s' if site.domain in settings.SSL_DOMAINS else '', site.domain) 
        get_dict = dict([(key, value) for (key, value) in request.GET.items()]) # ordinary dictionary of query parameters
                       
        openid_response = c.complete(get_dict, domain + request.path)
          
        if openid_response.status == consumer.SUCCESS:
            # Successful OpenID authorization 
            
            fr = ax.FetchResponse.fromSuccessResponse(openid_response)  
            email = fr.getSingle('http://axschema.org/contact/email').strip().lower()
            first_name = fr.getSingle('http://axschema.org/namePerson/first').strip()
            last_name = fr.getSingle('http://axschema.org/namePerson/last').strip()
            terms_agree = True              # if they've downloaded our plugin, they've already agreed to the terms
                        
            queryset = Alias.objects.filter(email=email)
            if queryset.count() > 0:
                alias = queryset[0]
                d = _login_alias(request, alias)
            else:
                # Register a new user
                from assist import register
                
                data = {
                    'email':        email,
                    'service_name': settings.FIREFOX_SERVICE_NAME,
                    'first_name':   first_name,
                    'last_name':    last_name,
                    'agent':        get_agent(request)
                }
                
                d = register.register(request, data)
                
                if d['success']:
                    queryset = Alias.objects.filter(email=email)
                    if queryset.count() > 0:
                        alias = queryset[0]
                        # Success - log that user in immediately
                        d = _login_alias(request, alias)
                    else:
                        # No alias after registration? Something's very wrong
                        d = {'success': False}                    
                else:
                    d = {'success': False}
   
        else:
            # Unsuccessful OpenID authorization, or user cancelled
            d = {'success': False}
            
        c = RequestContext(request, dict=d)
        
        t = loader.get_template('openid_return.html')
        
        #transaction.commit()                    # in case a registration took place
        return HttpResponse(t.render(c))    
            
    except:
        #transaction.rollback()
        log("openid_return: fail")
        log(traceback.format_exc())
        return HttpResponseRedirectAgent(request, '/')

        

