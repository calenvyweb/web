# Quick environment setup for Web10-Web20 servers!
echo "commencing installation" 

echo "Setting up the environment - upgrading Python eggs, etc."
#chsh -s /bin/bash calenvy

cd /usr/local/lib/python2.6/dist-packages/
sudo apt-get update
sudo apt-get install ntp
sudo apt-get -f -y install python-git
sudo apt-get -f -y install python-setuptools
sudo easy_install virtualenv
sudo apt-get -f -y install libpq-dev
sudo apt-get -f -y install python-dev
sudo easy_install -U pip
sudo easy_install -U http://www.djangoproject.com/download/1.1.2/tarball/
sudo pip install http://github.com/rackspace/python-cloudfiles/tarball/1.7.2
sudo apt-get -f -y install python-psycopg2
sudo easy_install -U pytz
sudo easy_install http://keyczar.googlecode.com/svn/trunk/python
sudo apt-get -f -y install python-pyasn1
sudo apt-get -f -y install subversion
sudo easy_install -U celery
sudo easy_install -U South
sudo easy_install -U django-tagging
sudo easy_install -U django-piston
sudo pip install -e git://github.com/nathanborror/django-basic-apps#egg=django-basic-apps
sudo pip install -e svn+https://calenvy.com/svn/payment/packages#egg=payment_client
sudo easy_install -U sorl-thumbnail
sudo easy_install -U django-filebrowser
sudo pip install BeautifulSoup==3.0.8.1
sudo easy_install -U icalendar
sudo easy_install -U jsmin
sudo pip install yolk
sudo easy_install -U gdata
sudo easy_install -U authorize
sudo easy_install -U dnspython
sudo pip install pycrypto
sudo pip install http://twhiteman.netfirms.com/pyDES/pyDes-2.0.1.zip
sudo pip install django-celery
sudo pip install oauth
sudo pip install http://keyczar.googlecode.com/files/python-keyczar-0.6b.061709.tar.gz
sudo pip install ghettoq
sudo pip install redis
sudo pip install python-memcached
sudo pip install beatbox

echo ""
echo "Setup complete! - Make sure you've added this Web's local IP to SQL0 and to /etc/postfix/main.cf on cron1 (to send emails)"

##### EMERGENCY #####
# to kill orphans: dpkg --purge `pgadmin3`