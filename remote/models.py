from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db import connection
from django.db.models import *
from django.contrib.auth.models import User
from django.template import Context, loader, Template
from django.contrib import admin
import hashlib
import re, string, os
import json
from utils import *
# from datetime import *
import datetime
from assist.constants import *
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
from django.utils.translation import ungettext
from django.utils.translation import ugettext as _
from assist.models import *
from assist import mail
from schedule.models import *
from pytz import UTC
import pdb, traceback, pprint
from paypal.standard.models import *
import urllib, urllib2, sys
from api.client import *
# import beatbox


class RemoteUserManager(Manager):
    def get_primary_for_alias(self, alias, type):
        try:
            return RemoteUser.objects.filter(alias=alias, type=type)[0]
        except:
            return None
        
    
        
class RemoteUser(Model):
    
    TYPE_SF = 'tSFO'                    # Incoming remote user: SF application talking to us
    TYPE_FIREFOX = 'tFFX'               
    TYPE_OUTLOOK = 'tOUT'
    TYPE_DESKTOP = 'tDSK'
    TYPE_CURECAL = 'tCAL'               # That's us!
    TYPE_FOLLOWUP = 'tFOL'              # Outgoing remote user: Followup server
    
    TYPES = [TYPE_SF, TYPE_FIREFOX, TYPE_OUTLOOK, TYPE_DESKTOP, TYPE_FOLLOWUP]
    
    TYPE_CATEGORIES = {                                                         # Bucket names for the types (used for the admin_ping)
        TYPE_SF: 'salesforce',
        TYPE_FIREFOX: 'firefox',
        TYPE_OUTLOOK: 'outlook',
        TYPE_DESKTOP: 'desktop'
    }
        
    type = models.CharField(max_length=4, null=True, blank=True)
    enabled = models.NullBooleanField(default=False, null=True, blank=True)
    valid = models.NullBooleanField(default=False, null=True, blank=True)
    username = models.CharField(max_length=255, null=True, blank=True)
    password_aes = models.CharField(max_length=255, null=True, blank=True)      # Currently not used
    token_aes = models.CharField(max_length=255, null=True, blank=True)         # Currently not used
    remote_userid = models.CharField(max_length=32, null=True, blank=True)
    remote_organizationid = models.CharField(max_length=32, null=True, blank=True)
    remote_useremail = models.CharField(max_length=128, null=True, blank=True)  # Currently not used
    last_updated = UTCDateTimeField(default=now)
    account = models.ForeignKey('assist.Account', null=True, blank=True)        # The account who owns this RemoteUser
    alias = models.ForeignKey('assist.Alias', null=True, blank=True)            # The alias who "owns" this remoteUser, if any
                                                                                #  (blank means it's an account-wide remote user)
                                                                                
    date_created = UTCDateTimeField(default=now)
    date_last_accessed = UTCDateTimeField(default=now)
    version = models.CharField(max_length=16, null=True, blank=True)
        
    host = models.CharField(max_length=128, null=True, blank=True)              # For external services, the host we connect to
    
    # TODO: Kill these fields (I think...)
    contacts_last_synced = UTCDateTimeField(null=True, blank=True)
    leads_last_synced = UTCDateTimeField(null=True, blank=True)
    emails_last_stored = UTCDateTimeField(null=True, blank=True)
    
    # The local contacts which correspond to this remote user
    contacts = models.ManyToManyField('assist.Contact', through='RemoteUserContact', related_name='remote_user_set')
    
    objects = RemoteUserManager()
    
    def get_api(self):
        """For external services only that have an API similar to ours, get the APIConsumer object
        Assumes a token has already been stored!"""
        
        assert(self.enabled and self.valid and self.type in [RemoteUser.TYPE_CURECAL, RemoteUser.TYPE_FOLLOWUP])
        
        credential = RemoteCredential.objects.get(remote_user=self)
        api = APIConsumer(domain=self.host, username=self.username, token=credential.token, anonymous=False)
        return api
    
        
    
  
class RemoteUserContact(Model):
    """The 'through' model for RemoteUser's M2M relationship to Contact
    Currently not used, except by the legacy Salesforce syncer
    TODO: Delete this"""
    
    TYPE_LEAD       =   '*LED'          # This Noted Contact is a Lead on Salesforce
    TYPE_CONTACT    =   '*CON'          # This Noted Contact is a Contact on Salesforce
    remote_user = models.ForeignKey(RemoteUser)
    contact = models.ForeignKey('assist.Contact')
    type = models.CharField(max_length=4, null=True, blank=True)
    date_created = UTCDateTimeField(default=now)
    remote_id = TruncCharField(max_length=32, null=True, blank=True)
    remote_owner_id = TruncCharField(max_length=32, null=True, blank=True)
    remote_account_id = TruncCharField(max_length=32, null=True, blank=True)
        

    
class RemoteCredentialManager(Manager):
    
    def get_or_create_for_remote_user(self, alias, remote_user, type, incoming=True, token=None):
        """
        Create a RemoteCredential for the given remote_user.
        alias can be None (account-wide RemoteUser)"""
        
        queryset = self.filter(alias=alias, remote_user=remote_user, type=type, incoming=incoming).order_by('-date_created')
        try:
            cred, created = queryset[0], False
        except IndexError:
            cred, created = RemoteCredential(alias=alias, remote_user=remote_user, type=type, incoming=incoming), True
            
            n = now()
            cred.type = type 
            cred.incoming = incoming
            if token:
                cred.token = token
            elif type == RemoteCredential.TYPE_LEGACY:
                cred.token = create_slug(length=20)
            elif type == RemoteCredential.TYPE_BASIC_AUTH_TOKEN:
                cred.token = create_slug(length=32)
            elif type == RemoteCredential.TYPE_WEB_ONETIME_TOKEN:
                cred.token = create_slug(length=32)
            else:
                raise NotImplementedError           # TODO: OAuth
            cred.date_created = n
            cred.date_last_accessed = n
            cred.date_expires = None
            cred.save()
        return cred, created
    

class RemoteCredential(Model):
    
    TYPE_LEGACY             =   'tLEG'              # Outlook, Salesforce, Firefox; can also be used for web login
    TYPE_BASIC_AUTH_TOKEN   =   'tBAS'              # Used in place of the password field for BasicAuth, but can't be used for web login
    TYPE_OAUTH_ACCESS_TOKEN =   'tOAA'              # All plugins going forward will use this
    TYPE_WEB_ONETIME_TOKEN  =   'tWEB'              # Token for logging into the website; can only be used once
    TYPE_WEB_ONETIME_TOKEN_USED  =   'tWBx'         # Used-up token for logging into the website

    TYPES = [TYPE_OAUTH_ACCESS_TOKEN, TYPE_BASIC_AUTH_TOKEN, TYPE_LEGACY, TYPE_WEB_ONETIME_TOKEN]
    
    type = models.CharField(max_length=4)
    incoming = models.BooleanField(default=True)    # A login credential (other widgets, plugins *into* our service?) For now, always True
    token = models.CharField(max_length=128, blank=True, null=True)
    alias = models.ForeignKey('assist.Alias', blank=True, null=True)
    date_created = UTCDateTimeField(default=now)
    date_last_accessed = UTCDateTimeField(blank=True, null=True)
    date_expires = UTCDateTimeField(blank=True, null=True)
    remote_user = models.ForeignKey(RemoteUser, blank=True, null=True)
    
    objects = RemoteCredentialManager()
    
    